PIX^ : PixUP
============

Script BASH pour téléverser des images vers des sites d'hébergements pour insertion dans forum.

Est capable d'utiliser les services de :

- Toile Libre, Debian-fr
- Lut.im, Framapic.org
- Casimages.com
- Hostingpics.net

S'utilise en mode console ou en mode graphique grâce à zenity.
PixUp peut aussi être appelé dans le gestionnaire de fichiers.

----

Dépendances : zenity, curl, et jq

----

INSTALLATION
------------
- Paquet deb:

https://framagit.org/PixUP/pixup/raw/master/pixup_3.0.1_all.deb

Appel de PixUp dans le gestionnaire de fichiers:
	Après sélection du fichier, faire un clic droit et choisir "Ouvrir avec..."
	Chercher dans la liste des applications et choisir "XPixUp".

- Dernière version du script:

Dans un terminal, déplacez-vous dans un répertoire de votre path (votre bin/ par exemple), puis copiez-coller et lancez cette ligne :

`wget -N -t 5 -T 10 https://framagit.org/PixUP/pixup/raw/master/pixup_3.0.1 && chmod +x pixup`

----

UTILISATION
-----------

./pixup

Aide : ./pixup -h

Forum
-----

http://forum.ubuntu-fr.org/viewtopic.php?id=1027431

Auteurs
-------

**Sébastien Charpentier** : "cracolinux" <cracolinux@mailoo.org><br />

Contributeurs/Mainteneurs
-------------------------

**Roger Sauret** : "erresse" <rogersauret@free.fr><br />
**HUC Stéphane** : "PengouinPdt" <devs@stephane-huc.net><br />

*Licence* : LPRAB http://sam.zoy.org/lprab/ ( ~ Domaine Public )

